/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.thanabodin.mavenproject1;

import java.util.Scanner;

/**
 *
 * @author fewsp
 */
class Example {

    static int add(int a, int b) {
        return a+b;
    }

    static String chup(char player1, char player2) {
        if(player1 =='s' && player2 == 'p'){
            return "P1";
        } else if(player1 =='h' && player2 == 's'){
            return "P1";
        } else if(player1 =='p' && player2 == 'h'){
            return "P1";
        } else if(player1 =='h' && player2 == 'p'){
            return "P2";
        } else if(player1 =='p' && player2 == 's'){
            return "P2";
        } else if(player1 =='s' && player2 == 'h'){
            return "P2";
        }
        return "draw";
    }
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.print("Please input player1: ");
        String player1 = sc.next();
        System.out.print("Please input player2: ");
        String player2 = sc.next();
        String winner = chup(player1.charAt(0), player2.charAt(0));
        System.out.println("Winner is "+winner);
    }
    
}
