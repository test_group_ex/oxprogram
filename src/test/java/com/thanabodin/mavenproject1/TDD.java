/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/UnitTests/JUnit5TestClass.java to edit this template
 */
package com.thanabodin.mavenproject1;

import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

/**
 *
 * @author fewsp
 */
public class TDD {
    
    public TDD() {
    }

    @Test
     public void testAdd_1_2is3() {
         assertEquals(3, Example.add(1, 2));
     }
     
     @Test
     public void testAdd_3_4is7() {
         assertEquals(7, Example.add(3, 4));
     }
     
     @Test
     public void testAdd_20_22is42() {
         assertEquals(42, Example.add(20, 22));
     }
     
     @Test
     public void testChop_P1_p_P2_p_is_draw(){
         assertEquals("draw", Example.chup('p','p'));
     }
     
     @Test
     public void testChop_P1_h_P2_h_is_draw(){
         assertEquals("draw", Example.chup('h','h'));
     }
     
     @Test
     public void testChop_P1_s_P2_s_is_draw(){
         assertEquals("draw", Example.chup('s','s'));
     }
     
     @Test
     public void testChop_P1_s_P2_p_is_P1(){
         assertEquals("P1", Example.chup('s','p'));
     }
     
     @Test
     public void testChop_P1_h_P2_s_is_P1(){
         assertEquals("P1", Example.chup('h','s'));
     }
     
     @Test
     public void testChop_P1_p_P2_h_is_P1(){
         assertEquals("P1", Example.chup('p','h'));
     }
     
     @Test
     public void testChop_P1_h_P2_p_is_P2(){
         assertEquals("P2", Example.chup('h','p'));
     }
     
     @Test
     public void testChop_P1_p_P2_s_is_P2(){
         assertEquals("P2", Example.chup('p','s'));
     }
     
     @Test
     public void testChop_P1_s_P2_h_is_P2(){
         assertEquals("P2", Example.chup('s','h'));
     }
}
